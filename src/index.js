/*

Contexto:
El equipo debe desarrollar una app que le permita a una persona subir fotos, comentar fotos y seguir a otros miembros de la comunidad.

Actividad 1
1. Cree un archivo json que contenga un listado de usuarios, nombre, username, clave y foto de perfil (link por ahora). -> Se agrego un elemento mas que es follow para almacenar los usuarios que se estan siguiendo para la ultima funcion (5)
2. Llamen el archivo json desde su app e impriman todos los usuarios conectados a la red.
3. Cree una función que le permita encontrar un usuario específico. Debe retornar el "perfil de la persona" (Nombre, foto)
4. Cree una función que le permita a un usuario cambiar su foto de perfil.
5. Cree una función que asocie a un usuario con otro. (Follow)


*/

const fs = require('fs');

let rawdata = fs.readFileSync('./src/usuarios.json');
let users = JSON.parse(rawdata);


console.log("Usuarios Conectados:")
for (const id in users) {
    console.log(users[id].username)
}


function findProfileByUsername(username){
    for (const id in users) {
        if (users[id].username == username) {
            const {name, photo} = users[id] 
            return {name,photo}
        }
    }
    return {"message": "No se ha encontrado el usuario"}
}

console.log(findProfileByUsername("usuario1"))

function changePhotoProfile(username,newPhoto) {
    for (const id in users) {
        if (users[id].username == username) {
            users[id].photo = newPhoto
            return {"message": "Foto cambiada correctamente"}
        }
    }
    return {"message": "No se ha encontrado el usuario"}
}

//console.log(changePhotoProfile("usuario1","https://google.cl"))
//console.log(users)

function followUser(username,userToFollow){
    let userToFollowFound= false;
    for (const id in users) {
        if (users[id].username == userToFollow) {
            userToFollowFound = true
        }}
    if (!userToFollowFound){
        return {"message": "No se ha encontrado el a seguir usuario"}
    }
    for (const id in users) {
        if (users[id].username == username) {
            users[id].follow.push(userToFollow)
            return {"message": "Usuario seguido correctamente"}
        }
    }
    return {"message": "No se ha encontrado el usuario"}
}


//console.log(followUser("usuario1","usuario2"))
//console.log(users)